<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:my="functions" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture" xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" mc:Ignorable="w14 wp14">
  <xsl:output method="xml" version="1.0" indent="no" encoding="UTF-8"/>

  <xsl:param name="base.dir" select="string('C:\Werkbestanden\Geonovum\Beheer\tpod_samenvoegen')"/>

  <!-- gebruikte directories -->
  <xsl:param name="temp.dir" select="fn:string-join((fn:tokenize($base.dir,'\\'),'temp'),'/')"/>

  <!-- gebruikte afbeeldingen -->
  <xsl:param name="media">
    <xsl:sequence select="collection(concat('file:/',$temp.dir,'/fragmenten?select=document.xml;recurse=yes'))//w:drawing[descendant::a:blip/@r:embed]"/>
  </xsl:param>

  <!-- bouw het document op -->

  <xsl:template match="w:body">
    <xsl:element name="{name()}">
      <xsl:apply-templates select="namespace::*|@*|node()"/>
      <xsl:apply-templates select="collection(concat('file:/',$temp.dir,'/fragmenten?select=document.xml;recurse=yes'))//w:body/node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="a:blip/@r:embed">
    <xsl:variable name="anchorId" select="ancestor::w:drawing/wp:inline/@wp14:anchorId"/>
    <xsl:variable name="eId" select="fn:index-of($media/w:drawing/wp:inline/@wp14:anchorId,$anchorId)"/>
    <xsl:attribute name="{name()}" select="fn:string-join(('eId',$eId))"/>
  </xsl:template>

  <!-- algemene templates -->

  <xsl:template match="element()">
    <xsl:element name="{name()}">
      <xsl:apply-templates select="namespace::*|@*|node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="namespace::*">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="@*">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="comment()|processing-instruction()">
    <xsl:copy-of select="."/>
  </xsl:template>

</xsl:stylesheet>